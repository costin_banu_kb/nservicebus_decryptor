﻿using KB.Crm.Encryption;
using Microsoft.Azure.KeyVault;
using Microsoft.Azure.Services.AppAuthentication;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace NServicebusDecryptor
{
    class Program
    {
        static async Task Main(string[] args)
        {
            string mid, kid, key, iv;

            Console.Write("message id: ");
            mid = Console.ReadLine();

            Console.Write("kid: ");
            kid = Console.ReadLine();

            Console.Write("key: ");
            key = Console.ReadLine();

            Console.Write("iv: ");
            iv = Console.ReadLine();

            var contents = await DownloadPageAsync(mid);
            Console.WriteLine(await Decrypt(contents, kid, key, iv));
        }

        static async Task<byte[]> DownloadPageAsync(string messageId)
        {
            DateTime date = DateTime.Today; //Date for which you want to run the monitoring
            DateTime time = DateTime.Now; //Date for which you want to run the monitoring
            string callbacksDateStr = date.ToString("dd_MMM_yyyy");
            string callbacksTimeStr = time.ToString("HH_mm_ss");

            //string baseFolder = @"C:\temp\";
            //string filePath = baseFolder + $"Result_{callbacksDateStr}_{callbacksTimeStr}.txt";
            //string finalFilePath = baseFolder + $"FinalResult_{callbacksDateStr}_{callbacksTimeStr}.txt";

            //File.Delete(filePath);
            //File.Delete(finalFilePath);

            // ... Target page download
            int total = 6; //provide error message count from service pulse
            int count = total / 50 + 1;
            for (int i = 1; i <= count; i++) //Total/50
            {
                string page = $"http://shared-prod-psc.westeurope.cloudapp.azure.com:33333/api/recoverability/groups/dbc58b0b-f24a-cf87-3795-a53916753d4a/errors?page={i}&sort=time_of_failure&status=unresolved";

                using HttpClient client = new HttpClient();
                using HttpResponseMessage response = await client.GetAsync(page);
                using HttpContent content = response.Content;
                string result = await content.ReadAsStringAsync();
                //using StreamWriter writer = new StreamWriter(filePath, true);
                //writer.WriteLine(result);
                

                var rootObjectResponse = JsonConvert.DeserializeObject<List<RootObject>>(result);
                //Console.WriteLine(i + ": " + rootObjectResponse.Count);
                foreach (var resp in rootObjectResponse)
                {

                    var sb = new StringBuilder();
                    sb.Append(resp.id + "*" + resp.exception.message + "*" + resp.time_of_failure + "*" + resp.message_type + "*");

                    ////Get body 
                    string page2 = $"http://shared-prod-psc.westeurope.cloudapp.azure.com:33333/api/messages/{resp.message_id}/body";

                    //Get body for that message only in which we are interested (message id from service pulse)
                    if (resp.message_id == messageId)
                    {
                        /*var bytes =*/return await client.GetByteArrayAsync(page2);
                        //var a = Encoding.GetEncoding("ISO-8859-1").GetString(bytes, 0, bytes.Count());
                        //File.WriteAllBytes(finalFilePath, bytes);

                    }
                }
            }

            return new byte[0];
        }

        static async Task<string> Decrypt(byte[] data, string kid, string key, string iv)
        {
            var azureServiceTokenProvider = new AzureServiceTokenProvider();
            var authenticationCallback = new KeyVaultClient.AuthenticationCallback(azureServiceTokenProvider.KeyVaultTokenCallback);
            var keyVaultClient = new KeyVaultClient(authenticationCallback);

            var service = new EncryptionService(keyVaultClient);

            var encryptedData = new EncryptedData(data, Convert.FromBase64String(key), Convert.FromBase64String(iv), kid);

            var decryptedData = await service.DecryptAsync(encryptedData);
            
            //Get actual data
            return Encoding.UTF8.GetString(decryptedData, 0, decryptedData.Length);
        }

        public class Exception
        {
            public string exception_type { get; set; }
            public string message { get; set; }
            public string source { get; set; }
            public string stack_trace { get; set; }
        }

        public class SendingEndpoint
        {
            public string name { get; set; }
            public string host_id { get; set; }
            public string host { get; set; }
        }

        public class ReceivingEndpoint
        {
            public string name { get; set; }
            public string host_id { get; set; }
            public string host { get; set; }
        }

        public class RootObject
        {
            public string id { get; set; }
            public string message_type { get; set; }
            public DateTime time_sent { get; set; }
            public bool is_system_message { get; set; }
            public Exception exception { get; set; }
            public string message_id { get; set; }
            public int number_of_processing_attempts { get; set; }
            public string status { get; set; }
            public SendingEndpoint sending_endpoint { get; set; }
            public ReceivingEndpoint receiving_endpoint { get; set; }
            public string queue_address { get; set; }
            public DateTime time_of_failure { get; set; }
            public DateTime last_modified { get; set; }
        }
    }
}
